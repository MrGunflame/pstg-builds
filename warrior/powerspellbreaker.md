### Optional Traits :

- Defence - <Trait name="Cull the Weak"/>(increases damage)  instead of <Trait name="Dogged March"/>.
- Discipline - <Trait name="Burst Mastery"/> when wielding a greatsword/ <Skill name="Axe Mastery"/> when wielding an axe offhand.
- Spellbreaker - <Trait name="No Escape"/>(immobilize target when you use Hammer F1) instead of <Trait name="Pure Strike"/> (damage).

### Optional Ultility SKillls :

<Skill name="Break Enchantments"/> can be replaced for <Skill name="Endure Pain"/>.
<Skill name="Berserker Stance"/> when there is too much condition damage also good to charge your adrenaline quickly.
<Skill name="//Shake It Off!//"/>( 2 condi clears for 5 targets including you and break stun isly for yourself) can replace <Skill name="Featherfoot Grace"/>.

### Notes :

Sword/Axe can be replaced for Greatsword (personal preference).
<Item name="Superior Rune of the Scholar"/> can be replaced for <Skill name="Superior Rune of Strength"/>
BERSERKER TRINKETS can be replaced by some VALKYRIE, KNIGHT or MARAUDER if you want more sustain.

### How to play?

THE  MAIN JOB FOR A WARRIOR IS TO PUT A PERFECT <Skill name="Winds of Disenchantment"/> (Bubble) on enemy and so in order to do that you need to synchronize it with your squad (call it in voice when you are about to BUBBLE on enemy also there are times when commander calls for it).
For a succesful bubble <Skill name="Balanced Stance" -> <Skill name="Featherfoot Grace"/> -> <Skill name="Winds of Disenchantment"/> (Bubble).
After bubble use <Skill name="Earthshaker"/>(for CC) -> <Skill name="Full Counter"/>(a succesful ful counter refreshes <Skill name="Earthshaker"/>) -> <Skill name="Earthshaker"/> then SWAP WEAPON if GREATSWORD then use <Skill name="Arcing Slice"/> -> <Skill name="Hundred Blades"/> -> <Skill name="Arcing Slice"/> / If on AXE then use <Skill name="Whirling Axe"/> -> <Skill name="Dual Strike"/> SWAP WEAPON and repeat it.
